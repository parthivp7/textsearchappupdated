export interface TextSearchState {
  inputText: string;
  queryInputs: string[];
  searchMode: 'batch' | 'online';
  results: string[];
}

export const initialState: TextSearchState = {
  inputText: 'Seaspan is a shipping company',
  queryInputs: [],
  searchMode: 'batch',
  results: []
};
